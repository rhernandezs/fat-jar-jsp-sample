FROM openjdk:11.0.6-jdk
LABEL usuario="rhernandezs"
WORKDIR /workspace
COPY target/calculadora-*.jar app.jar
EXPOSE 9090
ENTRYPOINT exec java -jar /workspace/app.jar
