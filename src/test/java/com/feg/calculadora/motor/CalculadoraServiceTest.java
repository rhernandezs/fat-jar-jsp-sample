package com.feg.calculadora.motor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.feg.calculadora.motor.CalculadoraServiceImpl;

public class CalculadoraServiceTest {

  private CalculadoraService calc = new CalculadoraServiceImpl();

  @Test
  public void deberiaInterpretarUnNumero() {
    assertEquals(1.0, calc.calcular("1"), 0.1);
    assertEquals(-1.0, calc.calcular("-1"), 0.1);
    assertEquals(.1, calc.calcular(".1"), 0.1);
  }

  @Test
  public void deberiaInterpretarUnaSumaSimple() {
    assertEquals(3.0, calc.calcular("1+2"), 0.1);
    assertEquals(1.0, calc.calcular("-1+2"), 0.1);
    assertEquals(-1.0, calc.calcular("1+-2"), 0.1);
  }

  @Test
  public void deberiaInterpretarUnaSumaCompuesta() {
    assertEquals(6.0, calc.calcular("1+2+3"), 0.1);
    assertEquals(4.0, calc.calcular("-1+2+3"), 0.1);
    assertEquals(2.0, calc.calcular("1+-2+3"), 0.1);
    assertEquals(0.0, calc.calcular("1+2+-3"), 0.1);
    assertEquals(-2.0, calc.calcular("-1+2+-3"), 0.1);
    assertEquals(-4.0, calc.calcular("1+-2+-3"), 0.1);
  }

  @Test
  public void deberiaInterpretarUnaRestaSimple() {
    assertEquals(-1.0, calc.calcular("1-2"), 0.1);
    assertEquals(-3.0, calc.calcular("-1-2"), 0.1);
    assertEquals(3.0, calc.calcular("1--2"), 0.1);
  }

  @Test
  public void deberiaInterpretarUnaRestaCompuesta() {
    assertEquals(6.0, calc.calcular("1+2+3"), 0.1);
    assertEquals(4.0, calc.calcular("-1+2+3"), 0.1);
    assertEquals(2.0, calc.calcular("1+-2+3"), 0.1);
    assertEquals(0.0, calc.calcular("1+2+-3"), 0.1);
    assertEquals(-2.0, calc.calcular("-1+2+-3"), 0.1);
    assertEquals(-4.0, calc.calcular("1+-2+-3"), 0.1);
  }

  @Test
  public void deberiaInterpretarUnaMultiplicacionSimple() {
    assertEquals(6.0, calc.calcular("2*3"), 0.1);
    assertEquals(-6.0, calc.calcular("-2*3"), 0.1);
    assertEquals(-6.0, calc.calcular("2*-3"), 0.1);
    assertEquals(6.0, calc.calcular("-2*-3"), 0.1);
  }

  @Test
  public void deberiaInterpretarUnaMultiplicacionCompuesta() {
    assertEquals(24.0, calc.calcular("2*3*4"), 0.1);
    assertEquals(-24.0, calc.calcular("-2*3*4"), 0.1);
    assertEquals(-24.0, calc.calcular("2*-3*4"), 0.1);
    assertEquals(24.0, calc.calcular("-2*-3*4"), 0.1);
    assertEquals(-24.0, calc.calcular("2*3*-4"), 0.1);
    assertEquals(24.0, calc.calcular("-2*3*-4"), 0.1);
    assertEquals(24.0, calc.calcular("2*-3*-4"), 0.1);
    assertEquals(-24.0, calc.calcular("-2*-3*-4"), 0.1);
  }

  @Test
  public void deberiaInterpretarUnaDivisionSimple() {
    assertEquals(2.0, calc.calcular("6/3"), 0.1);
    assertEquals(-2.0, calc.calcular("-6/3"), 0.1);
    assertEquals(-2.0, calc.calcular("6/-3"), 0.1);
    assertEquals(2.0, calc.calcular("-6/-3"), 0.1);
    assertTrue(calc.calcular("2/0").isInfinite());
  }

  @Test
  public void deberiaInterpretarUnaDivisionCompuesta() {
    assertEquals(1.0, calc.calcular("6/3/2"), 0.1);
    assertEquals(-1.0, calc.calcular("-6/3/2"), 0.1);
    assertEquals(-1.0, calc.calcular("6/-3/2"), 0.1);
    assertEquals(1.0, calc.calcular("-6/-3/2"), 0.1);
    assertEquals(-1.0, calc.calcular("6/3/-2"), 0.1);
    assertEquals(1.0, calc.calcular("-6/3/-2"), 0.1);
    assertEquals(1.0, calc.calcular("6/-3/-2"), 0.1);
    assertEquals(-1.0, calc.calcular("-6/-3/-2"), 0.1);
    assertTrue(calc.calcular("6/3/0").isInfinite());
    assertTrue(calc.calcular("6/0/2").isInfinite());
  }

  @Test
  public void deberiaInterpretarUnaExpresionAnidada() {
    assertEquals(6.0, calc.calcular("1+(2+3)"), 0.1);
    assertEquals(7.0, calc.calcular("1+(2*3)"), 0.1);
    assertEquals(5.0, calc.calcular("1*(2+3)"), 0.1);
    assertEquals(6.0, calc.calcular("1*(2*3)"), 0.1);
    assertEquals(6.0, calc.calcular("(2+3)+1"), 0.1);
    assertEquals(7.0, calc.calcular("(2*3)+1"), 0.1);
    assertEquals(5.0, calc.calcular("(2+3)*1"), 0.1);
    assertEquals(6.0, calc.calcular("(2*3)*1"), 0.1);

    assertEquals(3.0, calc.calcular("1+(6/3)"), 0.1);
    assertEquals(1.0, calc.calcular("5/(2+3)"), 0.1);
    assertEquals(1.0, calc.calcular("2/(6/3)"), 0.1);
    assertEquals(3.0, calc.calcular("(6/3)+1"), 0.1);
    assertEquals(1.0, calc.calcular("(2+3)/5"), 0.1);
    assertEquals(1.0, calc.calcular("(6/3)/2"), 0.1);
    assertTrue(calc.calcular("(6/0)/2").isInfinite());
    assertTrue(calc.calcular("(6/2)/0").isInfinite());

    assertEquals(2.0, calc.calcular("1+(-2+3)"), 0.1);
    assertEquals(-5.0, calc.calcular("1+(-2*3)"), 0.1);
    assertEquals(1.0, calc.calcular("1*(-2+3)"), 0.1);
    assertEquals(-6.0, calc.calcular("1*(-2*3)"), 0.1);
    assertEquals(2.0, calc.calcular("(-2+3)+1"), 0.1);
    assertEquals(-5.0, calc.calcular("(-2*3)+1"), 0.1);
    assertEquals(1.0, calc.calcular("(-2+3)*1"), 0.1);
    assertEquals(-6.0, calc.calcular("(-2*3)*1"), 0.1);

    assertEquals(-1.0, calc.calcular("1+(-6/3)"), 0.1);
    assertEquals(5.0, calc.calcular("5/(-2+3)"), 0.1);
    assertEquals(-1.0, calc.calcular("2/(-6/3)"), 0.1);
    assertEquals(-1.0, calc.calcular("(-6/3)+1"), 0.1);
    assertEquals(1.0, calc.calcular("(-2+3)/1"), 0.1);
    assertEquals(-1.0, calc.calcular("(-6/3)/2"), 0.1);
    assertTrue(calc.calcular("(-6/0)/2").isInfinite());
    assertTrue(calc.calcular("(6/-2)/0").isInfinite());
  }

  @Test
  public void deberiaInterpretarUnLogaritmoNatural() {
    assertEquals(0.0, calc.calcular("log(1)"), 0.1);
    assertEquals(0.0, calc.calcular("log(2-1)"), 0.1);
    assertEquals(0.0, calc.calcular("log(2/2)"), 0.1);
    assertEquals(0.0, calc.calcular("log(1*1)"), 0.1);
    assertTrue(calc.calcular("log(0)").isInfinite());
    assertTrue(calc.calcular("log(-1)").isNaN());
    assertEquals(2.0, calc.calcular("1+log(1)+1"), 0.1);
    assertEquals(2.0, calc.calcular("(1+log(1))+1"), 0.1);
    assertEquals(2.0, calc.calcular("1+(log(1)+1)"), 0.1);
  }

  @Test(expected = NumberFormatException.class)
  public void deberiaDevolverUnError1() {
    assertEquals(0.0, calc.calcular("1+"), 0.1);
  }

  @Test(expected = NumberFormatException.class)
  public void deberiaDevolverUnError2() {
    assertEquals(0.0, calc.calcular("1 1"), 0.1);
  }

  @Test(expected = NumberFormatException.class)
  public void deberiaDevolverUnError3() {
    assertEquals(0.0, calc.calcular("(1+1"), 0.1);
  }

  @Test(expected = NumberFormatException.class)
  public void deberiaDevolverUnError4() {
    assertEquals(0.0, calc.calcular("()+1"), 0.1);
  }

  @Test(expected = NumberFormatException.class)
  public void deberiaDevolverUnError5() {
    assertEquals(0.0, calc.calcular(". 1"), 0.1);
  }

  @Test(expected = NumberFormatException.class)
  public void deberiaDevolverUnError6() {
    assertEquals(0.0, calc.calcular("1. 1"), 0.1);
  }

  @Test
  public void deberiaCalcularLosRequerimientosDelEnunciado() {
    assertEquals(-225.0, calc.calcular("5*3*(8-23)"), 0.1);
    assertEquals(-225.0, calc.calcular("((5*3)*(8-23))"), 0.1);
  }

}
