package com.feg.calculadora.sesion;

import java.util.ArrayList;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.feg.calculadora.sesion.jpa.SesionRepository;

@Service
@Profile("test")
public class SesionServiceTestImpl extends SesionServiceImpl {

  public SesionServiceTestImpl(SesionRepository repo) {
    super(repo);
  }

  public void inicializarExpresiones() {
    expresiones = new ArrayList<String>();
  }
  
}
