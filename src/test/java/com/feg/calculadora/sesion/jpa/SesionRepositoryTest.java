package com.feg.calculadora.sesion.jpa;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SesionRepositoryTest {
  
  @Autowired
  private TestEntityManager em;

  @Autowired
  private SesionRepository repo;

  @Test
  public void deberiaRecuperarExpresionesOrdenadas() {
    List<Expresion> expresiones = new ArrayList<>();
    expresiones.add(new Expresion("3+3", 3));
    expresiones.add(new Expresion("1+1", 1));
    expresiones.add(new Expresion("2+2", 2));
    Sesion sesion = new Sesion("test", expresiones);
    em.persist(sesion);
    em.flush();
    em.detach(sesion);
   
    Sesion aux = repo.findOneByNombre("test");
   
    assertTrue(aux.getNombre().equals(sesion.getNombre()));
    assertTrue(aux.getExpresiones().get(0).getOrden().intValue() == 1);
    assertTrue(aux.getExpresiones().get(1).getOrden().intValue() == 2);
    assertTrue(aux.getExpresiones().get(2).getOrden().intValue() == 3);
  }
  
}
