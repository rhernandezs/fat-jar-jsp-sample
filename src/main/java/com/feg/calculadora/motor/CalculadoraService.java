package com.feg.calculadora.motor;

public interface CalculadoraService {

  Double calcular(String expr);
  
}
