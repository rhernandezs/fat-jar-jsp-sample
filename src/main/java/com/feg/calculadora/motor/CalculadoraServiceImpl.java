package com.feg.calculadora.motor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import com.feg.calculadora.motor.nodos.Division;
import com.feg.calculadora.motor.nodos.LogaritmoNatural;
import com.feg.calculadora.motor.nodos.Multiplicacion;
import com.feg.calculadora.motor.nodos.Nodo;
import com.feg.calculadora.motor.nodos.Numero;
import com.feg.calculadora.motor.nodos.Parentesis;
import com.feg.calculadora.motor.nodos.Resta;
import com.feg.calculadora.motor.nodos.Suma;

@Service
public class CalculadoraServiceImpl implements CalculadoraService {

  public Double calcular(String expr) {
    // detecto espacios conflictivos, que luego causarían un cálculo equivocado al eliminar espacios
    Pattern patternFormatoErroneo = Pattern.compile("([0-9]|\\.)\\s+([0-9]|\\\\.)");
    Matcher matcher = patternFormatoErroneo.matcher(expr);
    if (matcher.find()) {
      throw new NumberFormatException();
    }
    
    String aux = expr
      .toLowerCase()
      .replaceAll("\\s", "") // elimino espacios
      .replaceAll("^\\-", "_") // reemplazo signo "-" al inicio de la expresión por una representación interna del signo negativo
      .replaceAll("\\+\\-", "+_") // reemplazo "+-" por una representación interna del signo negativo
      .replaceAll("\\-\\-", "-_") // reemplazo "--" por una representación interna del signo negativo
      .replaceAll("\\*\\-", "*_") // reemplazo "*-" por una representación interna del signo negativo
      .replaceAll("\\/\\-", "/_") // reemplazo "/-" por una representación interna del signo negativo
      .replaceAll("\\(\\-", "(_"); // reemplazo "(-" por una representación interna del signo negativo
    
    return interpretar(aux).calcular();
  }

  private Nodo interpretar(String expr) {
    int nivel;
    Matcher matcher;

    // Identifico operadores +, -, o log, considerando anidamiento entre paréntesis
    Pattern patternSumaResta = Pattern.compile("(\\+|\\-|log\\(|\\(|\\))");
    matcher = patternSumaResta.matcher(expr);
    nivel = 0;
    
    while (matcher.find()) {
      switch (matcher.group()) {
        case "+":
          if (nivel == 0) {
            return new Suma(interpretar(expr.substring(0, matcher.start())), interpretar(expr.substring(matcher.end())));
          }
          break;
        case "-":
          if (nivel == 0) {
            return new Resta(interpretar(expr.substring(0, matcher.start())), interpretar(expr.substring(matcher.end())));
          }
          break;
        case "log(":
          nivel++;
          break;
        case "(":
          nivel++;
          break;
        case ")":
          nivel--;
          break;
      }
    }

    // Identifico operadores *, /, o log, considerando anidamiento entre paréntesis
    Pattern patternMultDiv = Pattern.compile("(\\*|\\/|log\\(|\\(|\\))");
    matcher = patternMultDiv.matcher(expr);
    nivel = 0;
    int posDiv = -1;
    
    while (matcher.find()) {
      switch (matcher.group()) {
        case "*":
          if (nivel == 0) {
            return new Multiplicacion(interpretar(expr.substring(0, matcher.start())), interpretar(expr.substring(matcher.end())));
          }
          break;
        case "/":
          if (nivel == 0) {
            // Se detectó la operación división, pero puede haber otra más a la derecha.
            posDiv = matcher.start();
          }
          break;
        case "log(":
          nivel++;
          break;
        case "(":
          nivel++;
          break;
        case ")":
          nivel--;
          break;
      }
    }

    // El caso de la división es diferente al resto de los operadores, ya que primero se tiene que "interpretar o evaluar" la
    // operación que se encuentra hacia la izquierda. Para el resto de los operadores es indistinto.
    if (posDiv > -1) {
      return new Division(interpretar(expr.substring(0, posDiv)), interpretar(expr.substring(posDiv + 1)));
    }
    else if (expr.startsWith("(") && expr.endsWith(")")) {
      return new Parentesis(interpretar(expr.substring(1, expr.length() - 1)));
    }
    else if (expr.startsWith("log(") && expr.endsWith(")")) {
      return new LogaritmoNatural(interpretar(expr.substring(4, expr.length() - 1)));
    }
    else {
      return new Numero(expr);
    }
  }

}
