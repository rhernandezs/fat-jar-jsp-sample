package com.feg.calculadora.motor.nodos;

public class Suma extends OperadorBinario implements Nodo {
  
  public Suma(Nodo operando1, Nodo operando2) {
    super(operando1, operando2);
  }

  @Override
  public Double calcular() {
    return operando1.calcular() + operando2.calcular();
  }

}
