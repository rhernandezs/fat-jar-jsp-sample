package com.feg.calculadora.motor.nodos;

public class Resta extends OperadorBinario implements Nodo {
  
  public Resta(Nodo operando1, Nodo operando2) {
    super(operando1, operando2);
  }

  @Override
  public Double calcular() {
    return operando1.calcular() - operando2.calcular();
  }

}
