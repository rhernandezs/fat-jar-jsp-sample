package com.feg.calculadora.motor.nodos;

public interface Nodo {
  
  Double calcular();
  
}
