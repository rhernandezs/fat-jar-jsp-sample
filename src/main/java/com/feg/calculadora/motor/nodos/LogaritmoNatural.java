package com.feg.calculadora.motor.nodos;

public class LogaritmoNatural implements Nodo {

  private Nodo nodo;
  
  public LogaritmoNatural(Nodo nodo) {
    this.nodo = nodo;
  }
  
  @Override
  public Double calcular() {
    return Math.log(nodo.calcular());
  }

}
