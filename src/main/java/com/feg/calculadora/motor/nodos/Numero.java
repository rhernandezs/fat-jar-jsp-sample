package com.feg.calculadora.motor.nodos;

public class Numero implements Nodo {

  private Double valor;
  
  public Numero(String valor) {
    this.valor = Double.valueOf(valor.replaceAll("\\_", "-"));
  }
  
  @Override
  public Double calcular() {
    return valor;
  }
  
}
