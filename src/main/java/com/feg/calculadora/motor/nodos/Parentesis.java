package com.feg.calculadora.motor.nodos;

public class Parentesis implements Nodo {

  private Nodo nodo;
  
  public Parentesis(Nodo nodo) {
    this.nodo = nodo;
  }
  
  @Override
  public Double calcular() {
    return nodo.calcular();
  }

}
