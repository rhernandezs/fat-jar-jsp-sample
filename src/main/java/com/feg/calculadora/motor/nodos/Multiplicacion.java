package com.feg.calculadora.motor.nodos;

public class Multiplicacion extends OperadorBinario implements Nodo {

  public Multiplicacion(Nodo operando1, Nodo operando2) {
    super(operando1, operando2);
  }

  @Override
  public Double calcular() {
    return operando1.calcular() * operando2.calcular();
  }

}
