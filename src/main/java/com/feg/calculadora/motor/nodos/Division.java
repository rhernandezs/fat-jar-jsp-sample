package com.feg.calculadora.motor.nodos;

public class Division extends OperadorBinario implements Nodo {
  
  public Division(Nodo operando1, Nodo operando2) {
    super(operando1, operando2);
  }

  @Override
  public Double calcular() {
    return operando1.calcular() / operando2.calcular();
  }

}
