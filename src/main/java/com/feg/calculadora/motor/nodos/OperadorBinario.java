package com.feg.calculadora.motor.nodos;

public abstract class OperadorBinario {

  protected Nodo operando1;
  protected Nodo operando2;
  
  public OperadorBinario(Nodo operando1, Nodo operando2) {
    this.operando1 = operando1;
    this.operando2 = operando2;
  }

}
