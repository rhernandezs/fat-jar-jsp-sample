package com.feg.calculadora.sesion;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

@Configuration
public class Configuracion {

  // Lista de expresiones enviadas a la calculadora por cada sesión web
  @Bean(name = "expresiones")
  @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
  public List<String> requestScopedBean() {
    return new ArrayList<>();
  }

}
