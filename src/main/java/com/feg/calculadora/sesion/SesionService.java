package com.feg.calculadora.sesion;

import java.util.List;

public interface SesionService {

  void agregarExpresion(String expr);
  
  void guardar(String nombre);
  
  List<String> recuperar(String nombre);

}
