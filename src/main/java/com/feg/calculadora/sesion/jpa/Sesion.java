package com.feg.calculadora.sesion.jpa;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "sesiones")
public class Sesion {

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Long id;

  @NotNull
  private String nombre;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
  @JoinColumn(name = "sesion_id")
  @OrderBy("orden ASC")
  private List<Expresion> expresiones;

  // Necesario para JPA
  public Sesion() {
  }

  public Sesion(String nombre, List<Expresion> expresiones) {
    this.nombre = nombre;
    this.expresiones = expresiones;
  }
  
  public String getNombre() {
    return nombre;
  }

  public List<Expresion> getExpresiones() {
    return expresiones;
  }
  
}
