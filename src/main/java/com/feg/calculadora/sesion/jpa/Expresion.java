package com.feg.calculadora.sesion.jpa;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "expresiones")
public class Expresion {

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Long id;

  @NotNull
  private String expr;

  @NotNull
  private Integer orden;

  // Necesario para JPA
  public Expresion() {
  }
  
  public Expresion(String expr, Integer orden) {
    this.expr = expr;
    this.orden = orden;
  }
  
  public String getExpr() {
    return expr;
  }

  public Integer getOrden() {
    return orden;
  }

}
