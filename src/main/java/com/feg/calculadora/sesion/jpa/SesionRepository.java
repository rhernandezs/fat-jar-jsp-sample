package com.feg.calculadora.sesion.jpa;

import org.springframework.data.repository.CrudRepository;

public interface SesionRepository extends CrudRepository<Sesion, String> {

  Sesion findOneByNombre(String nombre);
  
}
