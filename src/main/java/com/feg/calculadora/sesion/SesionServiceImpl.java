package com.feg.calculadora.sesion;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.feg.calculadora.sesion.jpa.Expresion;
import com.feg.calculadora.sesion.jpa.Sesion;
import com.feg.calculadora.sesion.jpa.SesionRepository;

@Service
@Profile("production")
public class SesionServiceImpl implements SesionService {

  private SesionRepository repo;
  
  @Resource(name = "expresiones")
  protected List<String> expresiones;

  @Autowired
  public SesionServiceImpl(SesionRepository repo) {
    this.repo = repo;
  }
  
  public void agregarExpresion(String expr) {
    expresiones.add(expr);
  }
  
  public void guardar(String nombre) {
    Sesion existente = repo.findOneByNombre(nombre);
    if (existente != null) {
      // Si existe, borro la sesión previamente persistida.
      repo.delete(existente);
    }
    
    List<Expresion> expresionesJPA = new ArrayList<>();

    int orden = 0;
    for (String e : expresiones) {
      expresionesJPA.add(new Expresion(e, ++orden));
    }

    Sesion sesion = new Sesion(nombre, expresionesJPA);
    repo.save(sesion);
  }
  
  public List<String> recuperar(String nombre) {
    Sesion sesion = repo.findOneByNombre(nombre);
    if (sesion != null) {
      expresiones.clear();
      sesion.getExpresiones().forEach(e -> expresiones.add(e.getExpr()));
      return expresiones;
    }
    else {
      return null;
    }
  }

}
