package com.feg.calculadora.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.feg.calculadora.motor.CalculadoraService;
import com.feg.calculadora.sesion.SesionService;

@RestController
@RequestMapping(consumes="text/plain", path="/calcular")
public class CalcularController {

  private CalculadoraService calc;
  private SesionService sesionSvc;

  @Autowired
  public CalcularController(CalculadoraService calc, SesionService sesionSvc) {
    this.calc = calc;
    this.sesionSvc = sesionSvc;
  }

  @PostMapping
  public String calcular(@RequestBody String expr) {
    sesionSvc.agregarExpresion(expr);
    return "= " + calc.calcular(expr);
  }

  @ExceptionHandler({NumberFormatException.class})
  public ResponseEntity<String> handleException() {
    return new ResponseEntity<>("No se pudo interpretar la expresion.", HttpStatus.BAD_REQUEST);
  }

}
