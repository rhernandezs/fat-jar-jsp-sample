package com.feg.calculadora.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.feg.calculadora.motor.CalculadoraService;
import com.feg.calculadora.sesion.SesionService;

@RestController
@RequestMapping(consumes="text/plain", path="/sesion")
public class SesionController {

  private CalculadoraService calc;
  private SesionService sesionSvc;
 
  @Autowired
  public SesionController(CalculadoraService calc, SesionService sesionSvc) {
    this.calc = calc;
    this.sesionSvc = sesionSvc;
  }
  
  @PostMapping(path="/guardar")
  public String guardar(@RequestBody String nombre) {
    sesionSvc.guardar(nombre);
    return "\"" + nombre + "\" almacenada.";
  }
  
  @PostMapping(path="/recuperar")
  public ResponseEntity<String> recuperar(@RequestBody String nombre) {
    List<String> expresiones = sesionSvc.recuperar(nombre);
    if (expresiones != null) {
      StringBuilder sb = new StringBuilder();
      
      expresiones.forEach(e -> {
        sb.append(e + "\n");
        try {
          sb.append("= " + calc.calcular(e) + "\n");
        }
        catch (NumberFormatException ex) {
          sb.append("No se pudo interpretar la expresion.\n");
        }
      });
      
      return new ResponseEntity<String>(sb.toString(), HttpStatus.OK);
    }
    else {
      return new ResponseEntity<String>("No encontrada.", HttpStatus.NOT_FOUND);
    }
  }

}
