# Ejercicio Calculadora

## Tecnologías utilizadas
- Maven
- Spring Boot 2.1.3
- Java 11
- H2 Database (in memory)

## Comandos Maven (utiliza Maven wrapper)
- Para testear\
`mvnw test`
- Para generar jar (fat jar)\
`mvnw package`
- Para ejecutar desde la línea de comandos\
`mvnw spring-boot:run`
- __Nota:__ en caso de contar con un servidor proxy para salida a internet, el mismo debe ser configurado en el archivo `jvm.config` dentro del directorio `.mvn`.

## Comandos curl para interactuar con el servicio REST desde la línea de comandos:
- Calcular\
`curl -b cookies.txt -c cookies.txt -X POST http://localhost:8080/calcular -H "Content-Type: text/plain" -d "(2+2)*log(10/3)"`\
`curl -b cookies.txt -c cookies.txt -X POST http://localhost:8080/calcular -H "Content-Type: text/plain" -d "5*3*(8-23)"`
- Guardar Sesión\
`curl -b cookies.txt -c cookies.txt -X POST http://localhost:8080/sesion/guardar -H "Content-Type: text/plain" -d "sesion1"`
- Recuperar Sesión\
`curl -b cookies.txt -c cookies.txt -X POST http://localhost:8080/sesion/recuperar -H "Content-Type: text/plain" -d "sesion1"`
